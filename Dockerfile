FROM ubuntu:xenial-20180417

RUN apt-get update && apt-get install -y python2.7
RUN apt-get install -y python-pip

RUN pip install tensorflow==1.4.1 && \
    pip install Cython &&  \
    pip install pillow &&  \
    pip install lxml && \
    pip install jupyter && \
    pip install matplotlib && \
    mkdir /src

WORKDIR /src 

RUN apt-get install -y git

RUN git clone --depth=1 https://github.com/tensorflow/models.git

RUN apt-get install -y python-tk


RUN apt-get install -y protobuf-compiler && \
    apt-get install -y python-pil && \
    apt-get install -y python-lxml

RUN sed -ie '87d' models/research/object_detection/protos/ssd.proto
RUN cd models/research/ && \
    bash -c 'protoc object_detection/protos/*.proto --python_out=.'
    

COPY ./helmet.py helmet.py

RUN bash -c 'PYTHONPATH=$PYTHONPATH:models/research/:models/research/slim python helmet.py --download-model'

ENTRYPOINT ["python", "./helmet.py", "-i"]

