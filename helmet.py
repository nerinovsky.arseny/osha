import numpy as np
import os
import sys
import tensorflow as tf
import zipfile

from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image

import time
import json

import argparse

# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("models/research")
sys.path.append("models/research/object_detection")
from object_detection.utils import ops as utils_ops

if tf.__version__ < '1.4.0':
  raise ImportError('Please upgrade your tensorflow installation to v1.4.* or later!')


# get_ipython().magic(u'matplotlib inline')


from utils import label_map_util
from utils import visualization_utils as vis_util

MODEL_NAME = 'faster_rcnn_inception_resnet_v2_atrous_oid_14_10_2017'
MODEL_FILE = MODEL_NAME + '.tar.gz'
DOWNLOAD_BASE = 'http://download.tensorflow.org/models/object_detection/'

PATH_TO_CKPT = 'faster_rcnn_inception_resnet_v2_atrous_oid/frozen_inference_graph.pb'

PATH_TO_LABELS = 'models/research/object_detection/data/oid_bbox_trainable_label_map.pbtxt'

NUM_CLASSES = 600


def download_model():
  import six.moves.urllib as urllib
  import tarfile

  opener = urllib.request.URLopener()
  opener.retrieve(DOWNLOAD_BASE + MODEL_FILE, MODEL_FILE)
  tar_file = tarfile.open(MODEL_FILE)
  for file in tar_file.getmembers():
    file_name = os.path.basename(file.name)
    if 'frozen_inference_graph.pb' in file_name:
      tar_file.extract(file, os.getcwd())

def load_graph():
  detection_graph = tf.Graph()
  with detection_graph.as_default():
    od_graph_def = tf.GraphDef()
    with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
      serialized_graph = fid.read()
      od_graph_def.ParseFromString(serialized_graph)
      tf.import_graph_def(od_graph_def, name='')
  return detection_graph

def load_image_into_numpy_array(image):
  (im_width, im_height) = image.size
  return np.array(image.getdata()).reshape(
      (im_height, im_width, 3)).astype(np.uint8)

def run_inference_for_single_image(image, graph):
  with graph.as_default():
    with tf.Session() as sess:
      # Get handles to input and output tensors
      ops = tf.get_default_graph().get_operations()
      all_tensor_names = {output.name for op in ops for output in op.outputs}
      tensor_dict = {}
      for key in [
          'num_detections', 'detection_boxes', 'detection_scores',
          'detection_classes', 'detection_masks'
      ]:
        tensor_name = key + ':0'
        if tensor_name in all_tensor_names:
          tensor_dict[key] = tf.get_default_graph().get_tensor_by_name(
              tensor_name)
      if 'detection_masks' in tensor_dict:
        # The following processing is only for single image
        detection_boxes = tf.squeeze(tensor_dict['detection_boxes'], [0])
        detection_masks = tf.squeeze(tensor_dict['detection_masks'], [0])
        # Reframe is required to translate mask from box coordinates to image coordinates and fit the image size.
        real_num_detection = tf.cast(tensor_dict['num_detections'][0], tf.int32)
        detection_boxes = tf.slice(detection_boxes, [0, 0], [real_num_detection, -1])
        detection_masks = tf.slice(detection_masks, [0, 0, 0], [real_num_detection, -1, -1])
        detection_masks_reframed = utils_ops.reframe_box_masks_to_image_masks(
            detection_masks, detection_boxes, image.shape[0], image.shape[1])
        detection_masks_reframed = tf.cast(
            tf.greater(detection_masks_reframed, 0.5), tf.uint8)
        # Follow the convention by adding back the batch dimension
        tensor_dict['detection_masks'] = tf.expand_dims(
            detection_masks_reframed, 0)
      image_tensor = tf.get_default_graph().get_tensor_by_name('image_tensor:0')

      # Run inference
      output_dict = sess.run(tensor_dict,
                             feed_dict={image_tensor: np.expand_dims(image, 0)})
      
      # print(output_dict)
      # all outputs are float32 numpy arrays, so convert types as appropriate
      output_dict['num_detections'] = int(output_dict['num_detections'][0])
      output_dict['detection_classes'] = output_dict[
          'detection_classes'][0].astype(np.uint32)
      output_dict['detection_boxes'] = output_dict['detection_boxes'][0]
      output_dict['detection_scores'] = output_dict['detection_scores'][0]
      if 'detection_masks' in output_dict:
        output_dict['detection_masks'] = output_dict['detection_masks'][0]
  return output_dict



def inference_on_image(opt, detection_graph, category_index):
    image = Image.open(opt.image_path)

    image = image.resize((300, 200), Image.ANTIALIAS)
    # the array based representation of the image will be used later in order to prepare the
    # result image with boxes and labels on it.
    image_np = load_image_into_numpy_array(image)
    # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
    image_np_expanded = np.expand_dims(image_np, axis=0)
    # Actual detection.
    b = time.time()
    output_dict = run_inference_for_single_image(image_np, detection_graph)
    e = time.time()
    res_time = e-b

    res = {
      'original_image' : opt.image_path,
      'time' : res_time,
      'helmet_present' : False
    }
    
    from collections import defaultdict
    out = defaultdict(list)
    min_score_thresh=.45
    xs =  zip(output_dict['detection_boxes'],
              output_dict['detection_classes'],
              output_dict['detection_scores'] )
    boxes, clses, scores = [], [], []

    t = filter(lambda x: category_index[x]['name'] == 'Helmet', category_index.keys())
    helmet_cls = list(t)[0]
    
    for box, cls, score in xs:
      if score < min_score_thresh:
        continue
      
      cls_name = category_index[cls]['name']
      if cls_name == 'Hat' or cls_name == 'Helmet':
        res['helmet_present'] = True

        boxes.append(box)
        clses.append(helmet_cls)
        scores.append(score)
    
    # Visualization of the results of a detection.
    if opt.image_with_boxes:
      vis_util.visualize_boxes_and_labels_on_image_array(
        image_np,
        np.array(boxes), np.array(clses), np.array(scores),
        category_index,
        #instance_masks=output_dict.get('detection_masks'),
        use_normalized_coordinates=True,
        line_thickness=2)

      img_with_boxes = Image.fromarray(image_np)

      res_image_path = opt.image_path.split('.')
      res_image_path = '.'.join(res_image_path[:-1] + ['with_boxes'] + [res_image_path[-1]] )

      img_with_boxes.save(res_image_path)
      res['image_with_boxes'] = res_image_path
    return res

def main():

  parser = argparse.ArgumentParser()
  parser.add_argument('--download-model', action='store_true',
                      help='Downloads TF model, and does nothing else')
  parser.add_argument('-n', '--no-bbox', action='store_false', dest='image_with_boxes',
                      help='Don\'t generate image with bounding boxes')
  parser.add_argument('-i', '--image_path', help='Path to classified image')
  
  opt = parser.parse_args()

  if opt.download_model:
    download_model()
  else:
    label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
    categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
    category_index = label_map_util.create_category_index(categories)

    graph = load_graph()
    res = inference_on_image(opt, graph,  category_index)

    print(json.dumps(res))

if __name__ == "__main__":
  main()
